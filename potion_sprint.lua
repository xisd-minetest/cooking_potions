local modname = minetest.get_current_modname()
local S, NS = cooking_potions.intllib

--
-- Potions data
--
local potion_id = "speed"
local potion_color = "green2"
local potion_description = S("Speed")
local potion_longdesc = S("Drinking it will make you faster for 30 seconds.")

local speed_default = cooking_potions.opt.player_stats.speed_default or 1
local speed_sprint = cooking_potions.opt.player_stats.speed_sprint or 2.5
local jump_default = cooking_potions.opt.player_stats.jump_default or 1
local jump_sprint = cooking_potions.opt.player_stats.jump_sprint or 1.2

cooking_potions.glob_id.fast = modname..':'..potion_id
cooking_potions.glob_effects.fast = {

	start = function(player)
		player:set_physics_override({
				speed = speed_sprint,
				jump = jump_sprint,
				-- gravity
			})
		-- player:set_physics_override(speed_sprint,jump_sprint,nil)
		-- Set potion speed attribute to be by globalstep function
		player:set_attribute("_cooking_potions_potion_fast","true")

	end,
	stop = function(effect, player)
		if not player then player = effect end
		player:set_physics_override({
				speed = speed_default,
				jump = jump_default,
				-- gravity
			})
		player:set_attribute("_cooking_potions_potion_fast",nil)
	end
}

--
-- Potion effect
--
-- Makes the user faster -- from playereffects exemples
playereffects.register_effect_type(potion_id, potion_description, "cooking_potions_"..potion_color..".png", {"speed"},  
	cooking_potions.glob_effects.fast.start,
	cooking_potions.glob_effects.fast.stop
)

--
-- Register Potion bottle item
--
cooking_potions.register_potion({
	basename = potion_id,
	color = potion_color,
	contentstring = potion_description,
	longdesc = potion_longdesc,
	effect_type = potion_id,
	duration = 30,
})

--
-- Register Potion craft recipe
--
local ingredients = {}
ingredients[1] = cooking_potions.waterbottle
ingredients[2] = "group:sapling"

local c = 2
local def = {
	type = "shapeless",
	output = modname..':'..potion_id,
	count = c,
	color = potion_color,
	recipe = {ingredients[1],ingredients[2]},
	recipe_pot = {ingredients[1]..' '..c,ingredients[2]},
}

cooking_potions.register_craft(def,true)

