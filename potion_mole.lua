local modname = minetest.get_current_modname()
local S = cooking_potions.intllib

--
-- Potions data
--
local potion_id = "mole"
local potion_color = "brown"
local potion_description = S("The Mole")
local potion_duration = 30
local potion_longdesc = S("For %d after drinking it, you will magically mine any two blocks in front of you horizontally, as if you were using a steel pickaxe on them.", potion_duration)
local potion_icon = "default_tool_steelpick.png" --"cooking_potions_"..potion_color..".png"


-- Funtions from pep mod

local peffect = {}
peffect.moles = {}

function peffect.enable_mole_mode(playername)
	peffect.moles[playername] = true
end

function peffect.disable_mole_mode(playername)
	peffect.moles[playername] = false
end


function peffect.yaw_to_vector(yaw)
	local tau = math.pi*2

	yaw = yaw % tau
	if yaw < tau/8 then
		return { x=0, y=0, z=1}
	elseif yaw < (3/8)*tau then
		return { x=-1, y=0, z=0 }
	elseif yaw < (5/8)*tau then
		return { x=0, y=0, z=-1 }
	elseif yaw < (7/8)*tau then
		return { x=1, y=0, z=0 }
	else
		return { x=0, y=0, z=1}
	end
end

function peffect.moledig(playername)
	local player = minetest.get_player_by_name(playername)

	local yaw = player:get_look_yaw()
	-- fix stupid oddity of Minetest, adding pi/2 to the actual player's look yaw...
	-- TODO: Remove this code as soon as Minetest fixes this.
	yaw = yaw - math.pi/2

	local pos = vector.round(player:getpos())

	local v = peffect.yaw_to_vector(yaw)

	local digpos1 = vector.add(pos, v)
	local digpos2 = { x = digpos1.x, y = digpos1.y+1, z = digpos1.z }

	local try_dig = function(pos)
		local n = minetest.get_node(pos)
		local ndef = minetest.registered_nodes[n.name]
		if ndef.walkable and ndef.diggable then
			if ndef.can_dig ~= nil then
				if ndef.can_dig() then
					return true
				else
					return false
				end
			else
				return true
			end
		else
			return false
		end
	end

	local dig = function(pos)
		if try_dig(pos) then
			local n = minetest.get_node(pos)
			local ndef = minetest.registered_nodes[n.name]
			if ndef.sounds ~= nil then
				minetest.sound_play(ndef.sounds.dug, { pos = pos })
			end
			-- TODO: Replace this code as soon Minetest removes support for this function
			local drops = minetest.get_node_drops(n.name, "default:pick_steel")
			minetest.dig_node(pos)
			local inv = player:get_inventory()
			local leftovers = {}
			for i=1,#drops do
				table.insert(leftovers, inv:add_item("main", drops[i]))
			end
			for i=1,#leftovers do
				minetest.add_item(pos, leftovers[i])
			end
		end
	end

	dig(digpos1)
	dig(digpos2)
end

peffect.timer = 0

minetest.register_globalstep(function(dtime)
	peffect.timer = peffect.timer + dtime
	if peffect.timer > 0.5 then
		for playername, is_mole in pairs(peffect.moles) do
			if is_mole then
				peffect.moledig(playername)
			end
		end
		peffect.timer = 0
	end
end)

--
-- Potion effect
--
-- User digs blocks by moving  -- From pep
playereffects.register_effect_type(potion_id, potion_description, potion_icon, {"autodig"},  
	function(player)
		peffect.enable_mole_mode(player:get_player_name())
	end,
	function(effect, player)
		peffect.disable_mole_mode(player:get_player_name())
	end
)

--
-- Register Potion bottle item
--
cooking_potions.register_potion({
	basename = potion_id,
	color = potion_color,
	contentstring = potion_description,
	longdesc = potion_longdesc,
	effect_type = potion_id,
	duration = potion_duration,
})



--
-- Register Potion craft recipe
--
local ingredients = {}
ingredients[1] = cooking_potions.waterbottle
ingredients[2] = "default:pick_steel"
ingredients[3] = "default:shovel_steel"
local c = 2
local def = {
	type = "shapeless",
	output = modname..':'..potion_id,
	count = c,
	color = potion_color,
	recipe = { ingredients[1],ingredients[2],ingredients[3] },

--[[	
	recipe = {
		{ingredients[3],"",ingredients[3]},
		{ingredients[3],ingredients[2],ingredients[3]},
		{"",ingredients[1],""},
		},
--]]
--	recipe_pot = {ingredients[1]..' '..c,ingredients[2],ingredients[3]},

}

cooking_potions.register_craft(def,true)

