-- This mod add potions effects and recipes
-- it depends on playereffects mod
-- and is very similar to pep (playereffects potions) mod
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Intllib
local S, NS = dofile(modpath.."/intllib.lua")


cooking_potions = {}
cooking_potions.intllib = S, NS
cooking_potions.bottle = "vessels:glass_bottle"
cooking_potions.drinking_glass = "vessels:drinking_glass"
cooking_potions.waterbottle = modname..":water"
cooking_potions.registered_potions_effects = {}
cooking_potions.opt = {}
cooking_potions.opt.craft_standard = minetest.settings:get_bool("cooking_potions.enable_standard_recipes") or true
cooking_potions.opt.craft_pot = minetest.settings:get_bool("cooking_potions.enable_cooking_pot_recipes") or true
cooking_potions.opt.player_stats = {
	speed_default = minetest.settings:get_bool("player.speed") or 1,
	speed_sprint = minetest.settings:get_bool("player.speed_sprint") or 2.5,
	jump_default = minetest.settings:get_bool("player.jump") or 1,
	jump_sprint = minetest.settings:get_bool("player.jump_sprint") or 1.2,
	jump_jump = minetest.settings:get_bool("player.jump_max") or 1.9,
	grav_default = minetest.settings:get_bool("player.grav") or 1,
	grav_jump = minetest.settings:get_bool("player.grav_jump") or 0.5,
}
cooking_potions.glob_effects = {}
cooking_potions.glob_id = {}
cooking_potions.items = {}


-- Register potion adapted from pep mod
cooking_potions.register_potion = function(potiondef)
	local on_use, desc
	
	if potiondef.on_use then
		on_use =  potiondef.on_use 
	else
		on_use = function(itemstack, user, pointed_thing)
			local effect
			if(potiondef.effect_type ~= nil) then
				effect = playereffects.apply_effect_type(potiondef.effect_type, potiondef.duration, user)
			end
			if not effect then return itemstack end
			itemstack:take_item()
			 minetest.sound_play("cooking_potions_gulp")
			 --~ minetest.sound_play("cooking_potions_gulp", {
					--~ to_player = name,
					--~ max_hear_distance = 5,
				--~ })
			local inv= user:get_inventory()
			if inv:room_for_item('main', cooking_potions.bottle) then 
				inv:add_item('main', cooking_potions.bottle) 
			else
				local pos = user:get_pos()
				minetest.add_item(pos, cooking_potions.bottle)
			end

			--~ if cooking_potions.awards_callback then cooking_potions.awards_callback(itemstack, user) end

			return itemstack
		end
	end
	if potiondef.description then
		desc = potiondef.description
	elseif potiondef.poison then
		desc = S("Poison of @1", potiondef.contentstring)
	else
		desc = S("Potion of @1", potiondef.contentstring)
	end
		
	minetest.register_node(modname..":"..potiondef.basename, {
		_doc_items_longdesc = potiondef.longdesc,
		_doc_items_usagehelp = S("Hold it in your hand, then left-click to drink it."),
		inventory_image = modname..'_'..potiondef.color..".png",
		wield_image = modname..'_'..potiondef.color..".png",
		description = desc,
		drawtype = "plantlike",
		--visual_scale = 1.0,
		_color = potiondef.color,
		_effect = {
			type =potiondef.effect_type,
			duration = potiondef.duration,
			},
		tiles = {modname..'_'..potiondef.color..".png"},
		paramtype = "light",
		is_ground_content = false,
		walkable = false,
		selection_box = {
			type = "fixed",
			fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
		},
		groups = {vessel = 1, dig_immediate = 3, attached_node = 1},
		sounds = default.node_sound_glass_defaults(),
	
		on_use = on_use,

	})
	
	local rpe = cooking_potions.registered_potions_effects
	rpe[#rpe+1] = { potiondef.effect_type, potiondef.duration }
	cooking_potions.registered_potions_effects = rpe
	
	
end

-- dofile(modpath.."/cooking_pot.lua")

cooking_potions.register_craft = function (def,force_standard,force_pot)

	if cooking_potions.opt.craft_standard or force_standard then
		minetest.register_craft({
				type = def.type,
				output = def.output,
				recipe = def.recipe
			})
	end
	if def.color and cooking_potions.register_pot_recipe 
	 and ( cooking_potions.opt.craft_pot or force_pot ) then
		
		if not def.recipe_pot then
			def.recipe_pot = def.recipe
		end
		cooking_potions.register_pot_recipe({
			type = def.type,
			output = def.output,
			count = def.count,
			color = def.color,
			recipe = def.recipe_pot,
			use_items = def.use_items,
		})
	end
end




cooking_potions.register_potion({
	basename = "water",
	color = "blue",
	description = S("Bottle of water"),
	longdesc = S("A glass bottle filled with water"),
	on_use = minetest.item_eat(1),
})

minetest.register_craft( {
	output = modname..":water 10",
	type = "shapeless",
	recipe = {"vessels:glass_bottle 10", "bucket:bucket_water"},
	replacements = {{"bucket:bucket_water", "bucket:bucket_empty"}},

})

minetest.override_item("vessels:glass_bottle", {
	liquids_pointable = true,
	on_use = function(itemstack, user, pointed_thing)
		print("using bottle")
		if pointed_thing.type == "object" then
			pointed_thing.ref:punch(user, 1.0, { full_punch_interval=1.0 }, nil)
			return user:get_wielded_item()
		elseif pointed_thing.type ~= "node" then
			print("not a node")
			-- do nothing if it's neither object nor node
			return
		end
		local p = pointed_thing.under
		-- Check if pointing to a liquid source
		local node = minetest.get_node(p)
		local nd = minetest.registered_items[node.name]
		local is_water, is_source
		if nd then
			--	local group = minetest.get_item_group(node.name, "water")
			is_water = ( nd.groups and nd.groups["water"] and nd.groups["water"] > 2 )
			-- is_source = ( nd.liquidtype and nd.liquidtype == "source" )
		end
--		if is_water and is_source then 
		if is_water then 
			local name = user:get_player_name()
			if minetest.is_protected(p, name) then return end
			-- Replace with water bottle
			local item_count = user:get_wielded_item():get_count()
			
			-- default set to return bottle
			local giving_back = modname..":water"

			-- check if holding more than 1 empty bottle
			if item_count > 1 then

				-- if space in inventory add filled bottle, otherwise drop as item
				local inv = user:get_inventory()
				if inv:room_for_item("main", {name=giving_back}) then
					inv:add_item("main", giving_back)
				else
					local pos = user:getpos()
					pos.y = math.floor(pos.y + 0.5)
					minetest.add_item(pos, giving_back)
				end

				-- set to return empty bottle minus 1
				giving_back = "vessels:glass_bottle "..tostring(item_count-1)

			end
			
			return ItemStack(giving_back)

		else
			-- non-liquid nodes will have their on_punch triggered
			if nd then
				nd.on_punch(pointed_thing.under, node, user, pointed_thing)
			end
			return user:get_wielded_item()
		end
	end, 
})
	
--~ dofile(modpath.."/potion_sprint.lua")
dofile(modpath.."/potion_invulnerability.lua")
dofile(modpath.."/potion_fly.lua")
dofile(modpath.."/potion_jump.lua")
dofile(modpath.."/potion_regen.lua")
dofile(modpath.."/potion_breath.lua")
dofile(modpath.."/potion_mole.lua")
-- dofile(modpath.."/potion_gigantisme.lua")
-- 
--~ dofile(modpath.."/items_effects.lua")
