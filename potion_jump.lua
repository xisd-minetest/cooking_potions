local modname = minetest.get_current_modname()
local S, NS = cooking_potions.intllib

--
-- Potions data
--
local potion_id = "jump"
local potion_color = "silver"
local potion_description = S("Jumping")
local potion_longdesc = S("Drinking it will make you jump higher")
local potion_duration = 30
local potion_icon = "cooking_potions_"..potion_color..".png"

local jump_default = cooking_potions.opt.player_stats.jump_default or 1
local jump_jump = cooking_potions.opt.player_stats.jump_jump or 1.9
local grav_default = cooking_potions.opt.player_stats.grav_default or 1
local grav_jump = cooking_potions.opt.player_stats.grav_jump or 0.5


cooking_potions.glob_id.jump = modname..':'..potion_id
cooking_potions.glob_effects.jump = {

	start = function(player)
--		player:set_physics_override(nil,2.3,nil)
		player:set_physics_override({
				-- speed = speed_default
				jump = jump_jump,
				gravity = grav_jump,
			})
		-- player:set_physics_override(nil,jump_jump,grav_jump)
	end,
	stop = function(effect, player)
		if not player then player = effect end
		player:set_physics_override({
				-- speed = speed_default
				jump = jump_default,
				gravity = grav_default,
			})
		--player:set_physics_override(nil,jump_default,grav_default)
	end
}

--
-- Potion effect
--
-- Makes the user jump higher -- from playereffects exemples
playereffects.register_effect_type(potion_id, potion_description, potion_icon, {"jump"},  
	cooking_potions.glob_effects.jump.start,
	cooking_potions.glob_effects.jump.stop
)

--
-- Register Potion bottle item
--
cooking_potions.register_potion({
	basename = potion_id,
	color = potion_color,
	contentstring = potion_description,
	longdesc = potion_longdesc,
	effect_type = potion_id,
	duration = potion_duration,
})

--
-- Register Potion craft recipe
--
local ingredients = {}
ingredients[1] = cooking_potions.waterbottle
--ingredients[2] = "default:obsidian_shard"
ingredients[2] = "farming:string"
ingredients[3] = "mobs:green_slimeball"
if not minetest.registered_items[ingredients[2]] then
	ingredients[2] = "default:apple"
end
if not minetest.registered_items[ingredients[3]] then
	ingredients[3] = "group:leaves"
end
local c = 2
local def = {
	--type = "shapeless",
	output = modname..':'..potion_id,
	count = c,
	color = potion_color,
	recipe = {
		{"",ingredients[3],""},
		{"",ingredients[2],""},
		{"",ingredients[1],""},
		},
--	recipe_pot = {ingredients[1]..' '..c,ingredients[2],ingredients[3]},
}

cooking_potions.register_craft(def,true)

