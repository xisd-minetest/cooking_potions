local modname = minetest.get_current_modname()
local S, NS = cooking_potions.intllib

--
-- Potions data
--
local potion_id = "regen"
local potion_color = "magenta"
local potion_description = S("Regeneration")
local potion_duration = 30
local potion_longdesc = S("Drinking it makes you regenerate health.")
local potion_icon = "heart.png" --"cooking_potions_"..potion_color..".png"

--
-- Potion effect
--
-- Regenerate health -- from playereffects exemples
playereffects.register_effect_type(potion_id, potion_description, potion_icon, {"regen"},  
	function(player)
		player:set_hp(player:get_hp()+1)
	end,
	nil, nil, nil, 1
)

--
-- Register Potion bottle item
--
cooking_potions.register_potion({
	basename = potion_id,
	color = potion_color,
	contentstring = potion_description,
	longdesc = potion_longdesc,
	effect_type = potion_id,
	duration = potion_duration,
})

--
-- Register Potion craft recipe
--
local ingredients = {}
ingredients[1] = cooking_potions.waterbottle
ingredients[2] = "default:apple"

local c = 2
local def = {
	type = "shapeless",
	output = modname..':'..potion_id,
	count = c,
	color = potion_color,
	recipe = {ingredients[1],ingredients[2]},
--	recipe_pot = {ingredients[1]..' '..c,ingredients[2],ingredients[3]},
}

cooking_potions.register_craft(def,true)

