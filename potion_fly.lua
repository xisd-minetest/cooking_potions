local modname = minetest.get_current_modname()
local S, NS = cooking_potions.intllib

--
-- Potions data
--
local potion_id = "fly"
local potion_color = "gcyan"
local potion_description = S("Flying")
local potion_longdesc = S("Drinking it will grant you the ability to fly ('K' to enter fly mode).")
local potion_duration = 60*3
local potion_icon = "mobs_feather.png" --"cooking_potions_"..potion_color..".png"

--
-- Potion effect
--
cooking_potions.glob_id.fly = modname..':'..potion_id
cooking_potions.glob_effects.fly = {

	start = function(player)
		local playername = player:get_player_name()
		local privs = minetest.get_player_privs(playername)

		-- if player already have fly privs, do not apply effect
		if privs.fly then
			local message = S("This potion would not have any effect since you already got the 'fly' privliège")
						.."\n "  S("(Use 'K' to enter fly mode)")
			minetest.chat_send_player(playername, message)
			return false
		elseif privs.privs then
			local message = S("You've got the privs priv, so you don't need a potion to grant yourself the 'fly' privliège")
			minetest.chat_send_player(playername, message)
			return false
		end
		player:set_attribute("_fly_effect_is_on.cooking_potions","true")
		privs.fly = true
		minetest.set_player_privs(playername, privs)
	end,
	stop = function(effect, player)
		local name 
		if effect and not player then 
			player = effect
			name = player:get_player_name()
		else 
			name = effect.playername
			player = minetest.get_player_by_name(name)
		end
		
		-- Remove allow fly attibutes
		player:set_attribute("_fly_effect_is_on.cooking_potions",nil)

		-- TODO: Do not override an other fly effect

		local privs = minetest.get_player_privs(name)
		-- Do not remove priv if player has the ability of changing privs
		--~ if privs and ( privs.creative or privs.privs ) then return end
		if privs and privs.privs then return end
		privs.fly = nil
		minetest.set_player_privs(name, privs)
	end
}

cooking_potions.allow_fly = function(player)
	local fly_flag = player:get_attribute("_fly_effect_is_on.cooking_potions")
	if ( fly_flag == "true" ) then return true
	else return false end
end


local enable_fly = cooking_potions.glob_effects.fly.start
local disable_fly = cooking_potions.glob_effects.fly.stop

-- Grant fly privilege
playereffects.register_effect_type(potion_id, potion_description, potion_icon, {"fly"},  
	enable_fly,
	disable_fly,
	false, -- not hidden
	false  -- do NOT cancel the effect on death
)
--~ -- Same but hidden ( for cape )
--~ playereffects.register_effect_type(potion_id.."_hidden", potion_description, potion_icon, {"fly"},  
	--~ enable_fly,
	--~ disable_fly,
	--~ true, -- hidden
	--~ true  -- cancel the effect on death
--~ )

--
-- Register Potion bottle item
--
cooking_potions.register_potion({
	basename = potion_id,
	color = potion_color,
	contentstring = potion_description,
	longdesc = potion_longdesc,
	effect_type = potion_id,
	duration = potion_duration,
})

--
-- Register Potion craft recipe
--
local ingredients = {}
ingredients[1] = cooking_potions.waterbottle
--ingredients[2] = "default:obsidian_shard"
ingredients[2] = "default:stick"
ingredients[3] = "mobs:feather"
if not minetest.registered_items[ingredients[3]] then
	ingredients[3] = "farming:string"
end
if not minetest.registered_items[ingredients[3]] then
	ingredients[3] = "default:apple"
end
local c = 2
local def = {
	--type = "shapeless",
	output = modname..':'..potion_id,
	count = c,
	color = potion_color,
	recipe = {
		{ingredients[3],"",ingredients[3]},
		{ingredients[3],ingredients[2],ingredients[3]},
		{"",ingredients[1],""},
		},
--	recipe_pot = {ingredients[1]..' '..c,ingredients[2],ingredients[3]},
}

cooking_potions.register_craft(def,true)

