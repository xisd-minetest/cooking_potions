local modname = minetest.get_current_modname()
local S = cooking_potions.intllib

--
-- Potions data
--
local potion_id = "breath"
local potion_color = "ggreen"
local potion_description = S("Breathing")
local potion_duration = 30
local potion_longdesc = S("Drinking it gives you breath underwater for %d seconds.",potion_duration )
local potion_icon = "bubble.png" --"cooking_potions_"..potion_color..".png"

--
-- Potion effect
--
-- Breath effect
playereffects.register_effect_type(potion_id, potion_description, potion_icon, {"breath"},  
	function(player)
		player:set_breath(player:get_breath()+2)
	end,
	nil, nil, nil, 1
)
--
-- Register Potion bottle item
--
cooking_potions.register_potion({
	basename = potion_id,
	color = potion_color,
	contentstring = potion_description,
	longdesc = potion_longdesc,
	effect_type = potion_id,
	duration = potion_duration,
})

--
-- Register Potion craft recipe
--
local ingredients = {}
ingredients[1] = cooking_potions.waterbottle
ingredients[2] = "default:papyrus"


local c = 2
local def = {
	type = "shapeless",
	output = modname..':'..potion_id,
	count = c,
	color = potion_color,
	recipe = { ingredients[2], ingredients[2], ingredients[2],
				ingredients[2], ingredients[2], ingredients[2],
				 ingredients[2], ingredients[1], ingredients[2] },
--	recipe_pot = {ingredients[1]..' '..c,ingredients[2],ingredients[3]},
}

cooking_potions.register_craft(def,true)

